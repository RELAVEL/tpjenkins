<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en « wp-config.php » et remplir les
 * valeurs.
 *
 * Ce fichier contient les réglages de configuration suivants :
 *
 * Réglages MySQL
 * Préfixe de table
 * Clés secrètes
 * Langue utilisée
 * ABSPATH
 *
 * @link https://fr.wordpress.org/support/article/editing-wp-config-php/.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define( 'DB_NAME', 'wordpress' );

/** Utilisateur de la base de données MySQL. */
define( 'DB_USER', 'root' );

/** Mot de passe de la base de données MySQL. */
define( 'DB_PASSWORD', '' );

/** Adresse de l’hébergement MySQL. */
define( 'DB_HOST', 'localhost' );

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/**
 * Type de collation de la base de données.
 * N’y touchez que si vous savez ce que vous faites.
 */
define( 'DB_COLLATE', '' );

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clés secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'QdXRx*dq+mcX}*Z{/ YCPQD{j]0@>LAB*%sV(S][8zv,xRpGI6z2g-R<rd {iyn7' );
define( 'SECURE_AUTH_KEY',  'kw`H$BP:bj#_%Mh-FC|s0/$XK]*c,>$#K=0?RcIav~*7?3<Ef:O$w}v((NGf6xq-' );
define( 'LOGGED_IN_KEY',    '[JG|i?Y93>iE.j=2+ZY^HJaX1CGG1tGqNBGA?jpSF5AmGCWLI&7{ZF!8w3[V!Rvo' );
define( 'NONCE_KEY',        '$lC)2:EpWF_XobAH0blM<v_uHiNvrGCyw!qsQ(.=G(+K mo8Dh(1L8P|6pb8])qL' );
define( 'AUTH_SALT',        '7Bm3fNGx>Krm or*Uc%2(d`)kqC9Z+ewz[9:LP<:Wir>b?kZ7M$aV8.MZ+RVl}xl' );
define( 'SECURE_AUTH_SALT', '7_HlADuK$g>eFT<pj$bo|LCIm]vT>@OaTu%X+ X!qw=1vt%LVehNzmU8PK5=~dM?' );
define( 'LOGGED_IN_SALT',   'zWHZ,ac+?|6.[ #rhM5XTe*k1<H_@UYr-4lkJ;I>/g>M%[dO}5@.B_%^/B0r.k27' );
define( 'NONCE_SALT',       'CNC~,XEnMu4~YV/NnmwH[uK| .7pg9geW[1O%TDeJ{nQb;gv%b>V2/sVc?BawLZT' );
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix = 'wp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortemment recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://fr.wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* C’est tout, ne touchez pas à ce qui suit ! Bonne publication. */

/** Chemin absolu vers le dossier de WordPress. */
if ( ! defined( 'ABSPATH' ) )
  define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once( ABSPATH . 'wp-settings.php' );
